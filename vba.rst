=====
 VBA
=====

.. highlight:: vbnet

Array
=====

Source: http://www.cpearson.com/excel/vbaarrays.htm

A static array is an array that is sized in the Dim statement that declares
the array::

    Dim StaticArray(1 To 10) As Variant
   
You cannot change the size or data type of a static array. When you Erase a
static array, no memory is freed. Erase simple set all the elements to their
default value (``0``, ``vbNullString``, ``Empty``, or ``Nothing``, depending
on the data type of the array).

A dynamic array is an array that is not sized in the Dim statement. Instead,
it is sized with the ReDim statement::

    Dim DynamicArray() As Long
    ReDim DynamicArray(1 To 10)
    
You can change the size of a dynamic array, but not the data type. When you
Erase a dynamic array, the memory allocated to the array is released. You must
``ReDim`` the array in order to use it after it has been Erased.

An array is allocated if it is either a static array or a dynamic array that
has been sized with the ``ReDim`` statement.
    
Static arrays are always allocated and never empty.
    
An array is empty or unallocated if it is a dynamic array that has not yet
been sized with the ``ReDim`` statement or that has been deallocated with the
Erase statement.  Static arrays are never unallocated or empty.

An element is one specific item in an array of items.



Save multiple sheets to a single PDF file
=========================================

http://stackoverflow.com/questions/14404650/excel-save-multiple-sheets-to-pdf

.. code-block:: vbnet

   ThisWorkbook.Sheets(Array("Sheet1", "Sheet2")).Select
    
   ActiveSheet.ExportAsFixedFormat Type:=xlTypePDF, Filename:= _
       "tempo.pdf", Quality:= xlQualityStandard, IncludeDocProperties:=True, _
        IgnorePrintAreas:=False, OpenAfterPublish:=True

.. note:: It seems if I put ``C:\`` in the ``Filename``, Excel raises an exception.

Count Participation and Attendance
==================================

Script to count participation and attendance scores. This script reads the cell
address from a seating map and then automatically generate formula to sum the
values up

.. toctree::
   :maxdepth: 1

   vba/count-participation-attendance

Generate Grade Handouts
=======================

Script to generate grade handouts from grade table.

.. toctree::
   :maxdepth: 1

   vba/generate-grade-handout



