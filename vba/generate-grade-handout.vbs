Sub generate_grade_handout()
    Dim Scores(1 To 10) As Variant

    sheet_main = "Exams"  ' sheet with student identifier. Data will be written to this sheet
    sheet_target = "Sheet1"

    Worksheets(sheet_main).Activate

    numRow = Range("A65536").End(xlUp).Row

    For student = 3 To numRow ' for each student
        Worksheets(sheet_main).Activate
    
        eid = Cells(student, 1).Value
        Name = Cells(student, 4).Value
        
        i = 1
        k = 14
        Scores(i + 0) = Cells(student, k + 0).Value
        Scores(i + 1) = Cells(student, k + 1).Value
        Scores(i + 2) = Cells(student, k + 2).Value
        Scores(i + 3) = Cells(student, k + 3).Value
        Scores(i + 4) = Cells(student, k + 4).Value
        Scores(i + 5) = Cells(student, k + 5).Value

        Worksheets(sheet_target).Activate
        Cells(3, 3).Value = Name
        Cells(4, 3).Value = eid

        k = 6
        Cells(k + 0, 4).Value = Scores(1)
        Cells(k + 1, 4).Value = Scores(2)
        Cells(k + 2, 4).Value = Scores(3)
        Cells(k + 3, 4).Value = Scores(4)
        Cells(k + 4, 4).Value = Scores(5)
        Cells(k + 5, 4).Value = Scores(6)

        ThisWorkbook.Sheets(Array(sheet_target)).Select

        ActiveSheet.ExportAsFixedFormat Type:=xlTypePDF, Filename:= _
            eid, Quality:= xlQualityStandard, IncludeDocProperties:=True, _
            IgnorePrintAreas:=False, OpenAfterPublish:=False

    Next student

End Sub